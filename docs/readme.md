---
home: true
heroText: EliteDomashka
heroImage: /assets/img/fulllogo.png
addres: play.in-story.org:19132
actionText: Розпочати
actionLink: /user
actions:
 - text: Посібник
   type: is-info is-light
   link: /user
 - text: Бот
   type: is-success 
   link: https://t.me/EliteDomashkaBot
features:
 - title: Записуй
   details: Завдяки <a>EliteDomashkaBot</a> злегкістю пиши д/з, прикріплюй вложення, редагуй, перенось.
 - title: OneClick ГДЗ
   details: Coming Soon
 - title: Споглядай
   details: Є классний чат? Гаразд, буду направляти в певний час всю домашню на ближчі дні. Навіть надам сайт під твій клас, який навіть буде доступний оффлайн. <a href="https://13.domashka.cloud">Приклад</a>
footer: 
---

